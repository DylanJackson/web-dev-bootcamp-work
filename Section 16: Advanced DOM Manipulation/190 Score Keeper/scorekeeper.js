//Sets button elements to variables for easy access
var buttons = document.getElementsByTagName("button");
var playerOneButton = buttons[0];
var playerTwoButton = buttons[1];
var resetButton = buttons[2];

var input = document.getElementsByTagName("input")[0];


var playerOneScore = 0;
var playerTwoScore = 0;
var maxScore = input.value;

var h1 = document.getElementsByTagName("h1")[0];

playerOneButton.addEventListener("click", function() {
    if (playerOneScore != maxScore && playerTwoScore != maxScore) {
        playerOneScore++;
        h1.textContent = playerOneScore + " - " + playerTwoScore;
        isWinner();
    }
});

playerTwoButton.addEventListener("click", function() {
    if (playerOneScore != maxScore && playerTwoScore != maxScore) {
        playerTwoScore++;
        h1.textContent = playerOneScore + " - " + playerTwoScore;
        isWinner();
    }
});

resetButton.addEventListener("click", function() {
    playerOneScore = 0;
    playerTwoScore = 0;
    h1.textContent = playerOneScore + " - " + playerTwoScore;
});

input.onchange = function() {
    if (input.value < 0) {
        input.value = 0;
    } else {
        maxScore = input.value;
        document.getElementsByTagName("p")[0].textContent = "Playing to: " + maxScore;
    }
};

function isWinner() {
    if (playerOneScore == maxScore) {
        h1.textContent = h1.textContent + " Player One Wins!";
    }
    if (playerTwoScore == maxScore) {
        h1.textContent = h1.textContent + " Player Two Wins!";
    }
}
