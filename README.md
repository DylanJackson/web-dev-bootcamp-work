As I work through [The Web Developer Bootcamp](https://www.udemy.com/course/the-web-developer-bootcamp/) I'll be uploading the results as a way to track progress.

My files may differ from the Instructor as I attempt to implement prior knowledge of programming to create a more code which is more readable and cohesive.
